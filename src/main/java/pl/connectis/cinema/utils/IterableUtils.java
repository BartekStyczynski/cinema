package pl.connectis.cinema.utils;

import java.util.ArrayList;
import java.util.List;

public class IterableUtils {

	public static <T> List<T> iterableToList(Iterable<T> iterable) {
		List<T> result = new ArrayList<>();
		iterable.forEach(result::add);
		return result;
	}
}
