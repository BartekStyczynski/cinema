package pl.connectis.cinema.infrastructure.adapter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.connectis.cinema.domain.model.Movie;
import pl.connectis.cinema.domain.model.MovieCategory;
import pl.connectis.cinema.domain.service.MovieRepository;
import pl.connectis.cinema.infrastructure.entity.MovieHibernate;
import pl.connectis.cinema.infrastructure.repository.MovieHibernateRepository;

@Repository
public class MovieRepositoryImpl implements MovieRepository {

	private final MovieHibernateRepository movieHibernateRepository;

	@Autowired
	public MovieRepositoryImpl(MovieHibernateRepository movieHibernateRepository) {
		this.movieHibernateRepository = movieHibernateRepository;
	}

	@Override
	public Movie createMovie(
			String title,
			MovieCategory category,
			Integer length,
			String description,
			Integer requiredAge) {
		MovieHibernate movieHibernate = new MovieHibernate(
				null,
				title,
				category,
				length,
				description,
				requiredAge
		);

		movieHibernateRepository.save(movieHibernate);
		return toDomain(movieHibernate);
	}

	@Override
	public Optional<Movie> getMovie(Long id) {
		return movieHibernateRepository.findById(id)
				.map(this::toDomain);
	}

	@Override
	public List<Movie> getAllMovies(Pageable pageable) {
		Page<MovieHibernate> page = movieHibernateRepository.findAll(pageable);
		List<MovieHibernate> hibernates = page.getContent();
		return hibernates.stream()
				.map(this::toDomain)
				.collect(Collectors.toList());
//		return movieHibernateRepository.findAll(pageable)
//				.get()
//				.map(this::toDomain)
//				.collect(Collectors.toList());
	}

	@Override
	public void removeMovie(Long id) {
		movieHibernateRepository.deleteById(id);
	}

	public Movie toDomain(MovieHibernate hibernate) {
		return new Movie(
				hibernate.getId(),
				hibernate.getTitle(),
				hibernate.getCategory(),
				hibernate.getLength(),
				hibernate.getDescription(),
				hibernate.getRequiredAge()
		);
	}
}
