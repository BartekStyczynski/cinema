package pl.connectis.cinema.infrastructure.adapter;

import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.connectis.cinema.domain.model.Marathon;
import pl.connectis.cinema.domain.service.MarathonRepository;
import pl.connectis.cinema.infrastructure.entity.MarathonHibernate;

@Repository
public class MarathonRepositoryImpl implements MarathonRepository {

	private final MovieRepositoryImpl movieRepository;

	@Autowired
	public MarathonRepositoryImpl(MovieRepositoryImpl movieRepository) {
		this.movieRepository = movieRepository;
	}

	public Marathon toDomain(MarathonHibernate hibernate) {
		return new Marathon(
				hibernate.getId(),
				hibernate.getName(),
				hibernate.getStartTime(),
				hibernate.getMovies()
						.stream()
						.map(movieRepository::toDomain)
						.collect(Collectors.toList())
		);
	}
}
