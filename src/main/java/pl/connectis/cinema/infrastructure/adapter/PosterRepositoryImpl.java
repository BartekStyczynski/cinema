package pl.connectis.cinema.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.connectis.cinema.domain.model.Movie;
import pl.connectis.cinema.domain.model.Poster;
import pl.connectis.cinema.domain.service.PosterRepository;
import pl.connectis.cinema.infrastructure.entity.PosterHibernate;
import pl.connectis.cinema.infrastructure.repository.MovieHibernateRepository;
import pl.connectis.cinema.infrastructure.repository.PosterHibernateRepository;

@Repository
public class PosterRepositoryImpl implements PosterRepository {

	private final MovieRepositoryImpl movieRepository;

	private final MovieHibernateRepository movieHibernateRepository;

	private final PosterHibernateRepository posterHibernateRepository;

	@Autowired
	public PosterRepositoryImpl(MovieRepositoryImpl movieRepository,
			MovieHibernateRepository movieHibernateRepository,
			PosterHibernateRepository posterHibernateRepository) {
		this.movieRepository = movieRepository;
		this.movieHibernateRepository = movieHibernateRepository;
		this.posterHibernateRepository = posterHibernateRepository;
	}

	@Override
	public Long createPoster(String filePath, Movie movie) {
		PosterHibernate posterHibernate = new PosterHibernate(
				null,
				filePath,
				movieHibernateRepository.findById(movie.getId()).get()
		);

		posterHibernateRepository.save(posterHibernate);
		return posterHibernate.getId();
	}

	public Poster toDomain(PosterHibernate hibernate) {
		return new Poster(
				hibernate.getId(),
				hibernate.getFilePath(),
				movieRepository.toDomain(hibernate.getMovie())
		);
	}
}
