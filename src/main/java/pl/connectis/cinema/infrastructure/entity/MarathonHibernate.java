package pl.connectis.cinema.infrastructure.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "marathon")
public class MarathonHibernate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String name;

	private LocalDateTime startTime;

	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(
			name = "marathon_movie",
			joinColumns = @JoinColumn(name = "marathon_id"),
			inverseJoinColumns = @JoinColumn(name = "movie_id"))
	private List<MovieHibernate> movies;

	public MarathonHibernate() {
	}

	public MarathonHibernate(
			Long id,
			String name,
			LocalDateTime startTime) {
		this.id = id;
		this.name = name;
		this.startTime = startTime;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public List<MovieHibernate> getMovies() {
		return movies;
	}

	public void setMovies(List<MovieHibernate> movies) {
		this.movies = movies;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MarathonHibernate that = (MarathonHibernate) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", MarathonHibernate.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("name='" + name + "'")
				.add("startTime=" + startTime)
				.toString();
	}
}
