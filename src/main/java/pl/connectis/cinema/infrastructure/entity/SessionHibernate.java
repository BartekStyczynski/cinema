package pl.connectis.cinema.infrastructure.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "session")
public class SessionHibernate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private LocalDateTime startTime;

	@ManyToOne
	@JoinColumn(name = "movie_id")
	private MovieHibernate movie;

	@ManyToOne
	@JoinColumn(name = "room_id")
	private RoomHibernate room;

	@OneToMany(mappedBy = "session", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<TicketHibernate> tickets;

	public SessionHibernate() {
	}

	public SessionHibernate(
			Long id,
			LocalDateTime startTime) {
		this.id = id;
		this.startTime = startTime;
	}

	public Long getId() {
		return id;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public MovieHibernate getMovie() {
		return movie;
	}

	public void setMovie(MovieHibernate movie) {
		this.movie = movie;
	}

	public RoomHibernate getRoom() {
		return room;
	}

	public void setRoom(RoomHibernate room) {
		this.room = room;
	}

	public List<TicketHibernate> getTickets() {
		if (tickets == null) {
			tickets = new ArrayList<>();
		}

		return tickets;
	}

	public void setTickets(List<TicketHibernate> tickets) {
		this.tickets = tickets;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SessionHibernate that = (SessionHibernate) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", SessionHibernate.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("startTime=" + startTime)
				.toString();
	}
}
