package pl.connectis.cinema.infrastructure.entity;

import java.util.Objects;
import java.util.StringJoiner;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "poster")
public class PosterHibernate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String filePath;

	@OneToOne
	@JoinColumn(name = "movie_id", referencedColumnName = "id")
	private MovieHibernate movie;

	public PosterHibernate() {
	}

	public PosterHibernate(Long id,
			String filePath,
			MovieHibernate movie) {
		this.id = id;
		this.filePath = filePath;
		this.movie = movie;
	}

	public Long getId() {
		return id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public MovieHibernate getMovie() {
		return movie;
	}

	public void setMovie(MovieHibernate movie) {
		this.movie = movie;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PosterHibernate that = (PosterHibernate) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", PosterHibernate.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("filePath='" + filePath + "'")
				.toString();
	}
}
