package pl.connectis.cinema.infrastructure.entity;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ticket")
public class TicketHibernate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String seat;

	private BigDecimal price;

	@ManyToOne
	@JoinColumn(name = "session_id")
	private SessionHibernate session;

	public TicketHibernate() {
	}

	public TicketHibernate(
			Long id,
			String seat,
			BigDecimal price) {
		this.id = id;
		this.seat = seat;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public SessionHibernate getSession() {
		return session;
	}

	public void setSession(SessionHibernate session) {
		this.session = session;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TicketHibernate that = (TicketHibernate) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", TicketHibernate.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("seat='" + seat + "'")
				.add("price=" + price)
				.toString();
	}
}
