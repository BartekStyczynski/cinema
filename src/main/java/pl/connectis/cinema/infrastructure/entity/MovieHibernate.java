package pl.connectis.cinema.infrastructure.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import pl.connectis.cinema.domain.model.MovieCategory;

@Entity
@Table(name = "movie")
public class MovieHibernate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String title;

	@Enumerated(EnumType.STRING)
	private MovieCategory category;

	private Integer length;

	private String description;

	private Integer requiredAge;

	@ManyToMany(mappedBy = "movies", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<MarathonHibernate> marathons;

	public MovieHibernate() {
	}

	public MovieHibernate(Long id,
			String title,
			MovieCategory category,
			Integer length,
			String description,
			Integer requiredAge) {
		this.id = id;
		this.title = title;
		this.category = category;
		this.length = length;
		this.description = description;
		this.requiredAge = requiredAge;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MovieCategory getCategory() {
		return category;
	}

	public void setCategory(MovieCategory category) {
		this.category = category;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getRequiredAge() {
		return requiredAge;
	}

	public void setRequiredAge(Integer requiredAge) {
		this.requiredAge = requiredAge;
	}

	public List<MarathonHibernate> getMarathons() {
		if (this.marathons == null) {
			this.marathons = new ArrayList<>();
		}

		return marathons;
	}

	public void setMarathons(List<MarathonHibernate> marathons) {
		this.marathons = marathons;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MovieHibernate that = (MovieHibernate) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", MovieHibernate.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("title='" + title + "'")
				.add("category=" + category)
				.add("length=" + length)
				.add("description='" + description + "'")
				.add("requiredAge=" + requiredAge)
				.toString();
	}
}
