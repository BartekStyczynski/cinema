package pl.connectis.cinema.infrastructure.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import pl.connectis.cinema.infrastructure.entity.MovieHibernate;
import pl.connectis.cinema.infrastructure.entity.PosterHibernate;

public interface PosterHibernateRepository
		extends CrudRepository<PosterHibernate, Long> {

	Optional<PosterHibernate> findByMovie(MovieHibernate movie);
}
