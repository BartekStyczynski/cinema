package pl.connectis.cinema.infrastructure.repository;

import org.springframework.data.repository.CrudRepository;
import pl.connectis.cinema.infrastructure.entity.RoomHibernate;

public interface RoomHibernateRepository
		extends CrudRepository<RoomHibernate, Long> {

}
