package pl.connectis.cinema.infrastructure.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.connectis.cinema.domain.model.MovieCategory;
import pl.connectis.cinema.infrastructure.entity.MovieHibernate;

public interface MovieHibernateRepository
		extends PagingAndSortingRepository<MovieHibernate, Long> {

	Page<MovieHibernate> findByCategory(MovieCategory category, Pageable pageable);

	Page<MovieHibernate> findByTitleContaining(String partOfTitle, Pageable pageable);
}
