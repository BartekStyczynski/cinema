package pl.connectis.cinema.infrastructure.repository;

import org.springframework.data.repository.CrudRepository;
import pl.connectis.cinema.infrastructure.entity.SessionHibernate;
import pl.connectis.cinema.infrastructure.entity.TicketHibernate;

public interface TicketHibernateRepository
		extends CrudRepository<TicketHibernate, Long> {

	Iterable<TicketHibernate> findAllBySession(SessionHibernate session);

	Long countBySession(SessionHibernate session);
}
