package pl.connectis.cinema.infrastructure.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.connectis.cinema.infrastructure.entity.SessionHibernate;

public interface SessionHibernateRepository
		extends JpaRepository<SessionHibernate, Long> {

	@Query(value = "SELECT s FROM SessionHibernate s WHERE function('DATE_TRUNC', 'day', s.startTime) = ?1")
	List<SessionHibernate> findAllByStartDate(Date date);

	@EntityGraph(value = "SessionHibernate.tickets", type = EntityGraph.EntityGraphType.LOAD)
	Optional<SessionHibernate> readById(Long id);
}
