package pl.connectis.cinema.infrastructure.repository;

import org.springframework.data.repository.CrudRepository;
import pl.connectis.cinema.infrastructure.entity.MarathonHibernate;

public interface MarathonHibernateRepository
		extends CrudRepository<MarathonHibernate, Long> {

}
