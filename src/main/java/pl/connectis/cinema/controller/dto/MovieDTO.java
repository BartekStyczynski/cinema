package pl.connectis.cinema.controller.dto;

import java.io.Serializable;
import pl.connectis.cinema.domain.model.Movie;
import pl.connectis.cinema.domain.model.MovieCategory;

public class MovieDTO implements Serializable {

	private Long id;

	private String title;

	private MovieCategory category;

	private Integer length;

	private String description;

	private Integer requiredAge;

	public MovieDTO() {
	}

	public MovieDTO(Movie movie) {
		this.id = movie.getId();
		this.title = movie.getTitle();
		this.category = movie.getCategory();
		this.length = movie.getLength();
		this.description = movie.getDescription();
		this.requiredAge = movie.getRequiredAge();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MovieCategory getCategory() {
		return category;
	}

	public void setCategory(MovieCategory category) {
		this.category = category;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getRequiredAge() {
		return requiredAge;
	}

	public void setRequiredAge(Integer requiredAge) {
		this.requiredAge = requiredAge;
	}

	public Movie toDomain() {
		return new Movie(
				id,
				title,
				category,
				length,
				description,
				requiredAge
		);
	}
}
