package pl.connectis.cinema.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.connectis.cinema.controller.dto.MovieDTO;
import pl.connectis.cinema.controller.exception.NotFoundException;
import pl.connectis.cinema.domain.model.Movie;
import pl.connectis.cinema.domain.service.MovieService;

@RestController
@RequestMapping("/movie")
public class MovieController {

	private final MovieService movieService;

	@Autowired
	public MovieController(MovieService movieService) {
		this.movieService = movieService;
	}

	@GetMapping(path = "/{id}")
	public MovieDTO getMovie(@PathVariable("id") Long id) {
		Optional<Movie> movieOptional = movieService.getMovie(id);
		if (!movieOptional.isPresent()) {
			throw new NotFoundException();
		}

		return new MovieDTO(movieOptional.get());
	}

	@PostMapping
	public Long createMovie(@RequestBody MovieDTO movieDTO) {
		Long movieId = movieService.createMovie(
				movieDTO.getTitle(),
				movieDTO.getCategory(),
				movieDTO.getLength(),
				movieDTO.getDescription(),
				movieDTO.getRequiredAge(),
				null
		);

		return movieId;
	}
}
