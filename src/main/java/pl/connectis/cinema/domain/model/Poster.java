package pl.connectis.cinema.domain.model;

import java.util.Objects;
import java.util.StringJoiner;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import pl.connectis.cinema.infrastructure.entity.MovieHibernate;

public class Poster {

	private Long id;

	private String filePath;

	private Movie movie;

	public Poster(Long id, String filePath, Movie movie) {
		this.id = id;
		this.filePath = filePath;
		this.movie = movie;
	}

	public Poster(String filePath, Movie movie) {
		this.filePath = filePath;
		this.movie = movie;
	}

	public Long getId() {
		return id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Poster that = (Poster) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Poster.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("filePath='" + filePath + "'")
				.toString();
	}
}
