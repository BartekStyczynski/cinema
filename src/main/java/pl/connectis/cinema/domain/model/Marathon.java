package pl.connectis.cinema.domain.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import pl.connectis.cinema.infrastructure.entity.MovieHibernate;

public class Marathon {

	private Long id;

	private String name;

	private LocalDateTime startTime;

	private List<Movie> movies;

	public Marathon(Long id, String name, LocalDateTime startTime, List<Movie> movies) {
		this.id = id;
		this.name = name;
		this.startTime = startTime;
		this.movies = movies;
	}

	public Marathon(String name, LocalDateTime startTime, List<Movie> movies) {
		this.name = name;
		this.startTime = startTime;
		this.movies = movies;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Marathon that = (Marathon) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Marathon.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("name='" + name + "'")
				.add("startTime=" + startTime)
				.toString();
	}
}
