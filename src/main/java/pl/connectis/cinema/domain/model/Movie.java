package pl.connectis.cinema.domain.model;

import java.util.Objects;
import java.util.StringJoiner;

public class Movie {

	private Long id;

	private String title;

	private MovieCategory category;

	private Integer length;

	private String description;

	private Integer requiredAge;

	public Movie(Long id,
			String title,
			MovieCategory category,
			Integer length,
			String description,
			Integer requiredAge) {
		this.id = id;
		this.title = title;
		this.category = category;
		this.length = length;
		this.description = description;
		this.requiredAge = requiredAge;
	}

	public Movie(String title,
			MovieCategory category,
			Integer length,
			String description,
			Integer requiredAge) {
		this.title = title;
		this.category = category;
		this.length = length;
		this.description = description;
		this.requiredAge = requiredAge;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MovieCategory getCategory() {
		return category;
	}

	public void setCategory(MovieCategory category) {
		this.category = category;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getRequiredAge() {
		return requiredAge;
	}

	public void setRequiredAge(Integer requiredAge) {
		this.requiredAge = requiredAge;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Movie that = (Movie) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Movie.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("title='" + title + "'")
				.add("category=" + category)
				.add("length=" + length)
				.add("description='" + description + "'")
				.add("requiredAge=" + requiredAge)
				.toString();
	}
}
