package pl.connectis.cinema.domain.model;

public enum MovieCategory {
	HORROR,
	THRILLER,
	DRAMA,
	COMEDY,
	WESTERN,
	ROMANTIC,
	ACTION,
	FAMILY
}
