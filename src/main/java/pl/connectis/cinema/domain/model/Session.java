package pl.connectis.cinema.domain.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Session {

	private Long id;

	private LocalDateTime startTime;

	private Movie movie;

	private Room room;

	private List<Ticket> tickets;

	public Session(Long id, LocalDateTime startTime, Movie movie, Room room, List<Ticket> tickets) {
		this.id = id;
		this.startTime = startTime;
		this.movie = movie;
		this.room = room;
		this.tickets = tickets;
	}

	public Session(LocalDateTime startTime, Movie movie, Room room, List<Ticket> tickets) {
		this.startTime = startTime;
		this.movie = movie;
		this.room = room;
		this.tickets = tickets;
	}

	public Long getId() {
		return id;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public List<Ticket> getTickets() {
		if (tickets == null) {
			tickets = new ArrayList<>();
		}

		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Session that = (Session) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Session.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("startTime=" + startTime)
				.toString();
	}
}
