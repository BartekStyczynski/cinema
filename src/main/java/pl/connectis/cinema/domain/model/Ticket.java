package pl.connectis.cinema.domain.model;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;

public class Ticket {

	private Long id;

	private String seat;

	private BigDecimal price;

	private Session session;

	public Ticket(Long id, String seat, BigDecimal price, Session session) {
		this.id = id;
		this.seat = seat;
		this.price = price;
		this.session = session;
	}

	public Ticket(String seat, BigDecimal price, Session session) {
		this.seat = seat;
		this.price = price;
		this.session = session;
	}

	public Long getId() {
		return id;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Ticket that = (Ticket) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Ticket.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("seat='" + seat + "'")
				.add("price=" + price)
				.toString();
	}
}
