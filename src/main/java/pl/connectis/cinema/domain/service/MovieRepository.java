package pl.connectis.cinema.domain.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import pl.connectis.cinema.domain.model.Movie;
import pl.connectis.cinema.domain.model.MovieCategory;

public interface MovieRepository {

	Movie createMovie(
			String title,
			MovieCategory category,
			Integer length,
			String description,
			Integer requiredAge
	);

	Optional<Movie> getMovie(Long id);

	List<Movie> getAllMovies(Pageable pageable);

	void removeMovie(Long id);
}
