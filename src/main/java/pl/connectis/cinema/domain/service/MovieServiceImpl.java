package pl.connectis.cinema.domain.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.connectis.cinema.domain.model.Movie;
import pl.connectis.cinema.domain.model.MovieCategory;

@Service
public class MovieServiceImpl implements MovieService {

	private final MovieRepository movieRepository;

	private final PosterRepository posterRepository;

	@Autowired
	public MovieServiceImpl(MovieRepository movieRepository,
			PosterRepository posterRepository) {
		this.movieRepository = movieRepository;
		this.posterRepository = posterRepository;
	}

	@Override
	public Long createMovie(
			String title,
			MovieCategory category,
			Integer length,
			String description,
			Integer requiredAge,
			String posterFilePath) {

		Movie movie = movieRepository.createMovie(
				title,
				category,
				length,
				description,
				requiredAge);

		if (posterFilePath != null) {
			posterRepository.createPoster(posterFilePath, movie);
		}

		return movie.getId();
	}

	@Override
	public Optional<Movie> getMovie(Long id) {
		return movieRepository.getMovie(id);
	}

	@Override
	public List<Movie> getAllMovies(Pageable pageable) {
		return movieRepository.getAllMovies(pageable);
	}

	@Override
	public void removeMovie(Long id) {
		movieRepository.removeMovie(id);
	}
}
