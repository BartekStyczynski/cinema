package pl.connectis.cinema.domain.service;

import pl.connectis.cinema.domain.model.Movie;

public interface PosterRepository {

	Long createPoster(String filePath, Movie movie);
}
